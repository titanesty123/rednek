export const actions = {
  async fetch({commit}) {
    const users = await this.$axios.$get('https://jsonplaceholder.typicode.com/albums/1/photos')
    commit('setUsers', users)
  }
}

export const mutations = {
  setUsers(state, users) {
    state.users = users.slice(0, 10)
  }
}

export const state = () => ({
  users: []
})

