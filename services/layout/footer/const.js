import { PhoneFormatter } from '~/helpers';
import { DocumentsApiService } from '~/services/common';

const data = Object.freeze({
  ru: {
    'main_header': 'DDoS-Guard',
    'social_network_header': 'Присоединяйтесь к нам:',
    'bottom_text': '© 2011-2022 DDoS-Guard. Все права защищены',
    'id': 1,
    'phones': [
      {
        'phone': '88003331763',
        'formatted': PhoneFormatter('88003331763', 'ru'),
      },
      {
        'phone': '84952150387',
        'formatted': PhoneFormatter('84952150387', 'ru'),
      },
    ],
    'emails': [
      {
        'email': 'info@ddos-guard.net',
      },
    ],
    'social_networks': [
      {
        'title': 'Linkedin',
        'link': 'https://www.linkedin.com/company/5039211',
        'social_network_code': 'linkedin',
      },
      {
        'title': 'Telegram',
        'link': 'https://t.me/ddos_guard',
        'social_network_code': 'telegram',
      },
    ],
    'payments': [
      {
        'title': 'Visa',
        'id': 1,
        'icon': {
          'url': 'Visa_b41b98da80.svg',
        },
      },
      {
        'title': 'Mastercard',
        'id': 2,
        'icon': {
          'url': 'Mastercard_96aabf074f.svg',
        },
      },
      {
        'title': 'PayMaster',
        'id': 4,
        'icon': {
          'url': 'Pay_Master_8d7e5e9ace.svg',
        },
      },
      {
        'title': 'Robokassa',
        'id': 5,
        'icon': {
          'url': 'Robbokassa_07b8a474ba.svg',
        },
      },
      {
        'title': 'Юkassa',
        'id': 6,
        'icon': {
          'url': 'Yukassa_5b7a6c4297.svg',
        },
      },
    ],
  },
  en: {
    'main_header': 'DDoS-Guard',
    'social_network_header': 'Join us:',
    'bottom_text': '© 2011-2022 DDoS-Guard. All Rights Reserved',
    'id': 2,
    'phones': [
      {
        'phone': '+551146733474',
        'formatted': PhoneFormatter('+551146733474', 'en'),
      },
    ],
    'emails': [
      {
        'email': 'info@ddos-guard.net',
      },
    ],
    'social_networks': [
      {
        'title': 'Telegram',
        'link': 'https://t.me/ddos_guard',
        'social_network_code': 'telegram',
      },
      {
        'title': 'Linkedin',
        'link': 'https://www.linkedin.com/company/5039211',
        'social_network_code': 'linkedin',
      },
    ],
    'payments': [
      {
        'title': 'Visa',
        'id': 7,
        'icon': {
          'url': 'Visa_b41b98da80.svg',
        },
      },
      {
        'title': 'Mastercard',
        'id': 9,
        'icon': {
          'url': 'Mastercard_96aabf074f.svg',
        },
      },
      {
        'title': 'PayMaster',
        'id': 13,
        'icon': {
          'url': 'Pay_Master_8d7e5e9ace.svg',
        },
      },
      {
        'title': 'Robokassa',
        'id': 15,
        'icon': {
          'url': 'Robbokassa_07b8a474ba.svg',
        },
      },
      {
        'title': 'Юkassa',
        'id': 17,
        'icon': {
          'url': 'Yukassa_5b7a6c4297.svg',
        },
      },
    ],
  },
});

const documents = {
  ru: DocumentsApiService.getDocuments('ru').map((document) => {
    return {
      ...document,
      remote_path: 1,
    };
  }),
  en: DocumentsApiService.getDocuments('en').map((document) => {
    return {
      ...document,
      remote_path: 1,
    };
  }),
};

const navigations = Object.freeze({
  ru: [
    {
      key: 'our_services',
      title: 'Продукты и решения',
      sublinks: [
        {
          key: 'web',
          title: 'Защита и ускорение сайтов',
          path: '/store/web',
        },
        {
          key: 'hosting',
          title: 'Защищенный хостинг',
          path: '/store/hosting',
        },
        {
          key: 'vds',
          title: 'Защищенный VDS',
          path: '/store/vds',
        },
        {
          key: 'server',
          title: 'Защищенные выделенные сервера',
          path: '/store/server',
        },
        {
          key: 'network_protection',
          title: 'Защита Сети',
          path: '/store/network-protection',
        },
        {
          key: 'bot-mitigation',
          title: 'Bot Mitigation',
          path: '/store/bot-mitigation',
        },
        {
          key: 'waf',
          title: 'WAF',
          path: '/store/waf',
        },
        // {
        //   key: 'ssl',
        //   title: 'SSL-сертификаты',
        //   path: '/store/ssl',
        // },
      ],
    },
    {
      key: 'support',
      title: 'Поддержка',
      sublinks: [
        {
          key: 'manual',
          title: 'Инструкции',
          path: '/wiki/manual',
        },
        ...documents.ru,
        {
          key: 'terminology',
          title: 'Термины',
          path: '/wiki/terminology',
        },
        {
          key: 'technologies',
          title: 'Технологии',
          path: '/wiki/technologies',
        },
      ],
    },
    {
      key: 'ddos-guard',
      title: 'DDoS-Guard',
      sublinks: [
        {
          key: 'about_us',
          title: 'О компании',
          path: '/info/about-us',
        },
        {
          key: 'vacancy',
          title: 'Вакансии',
          path: '/info/vacancy',
        },
        {
          key: 'blog',
          title: 'Блог',
          path: '/info/blog',
        },
        {
          key: 'contacts',
          title: 'Контакты',
          path: '/info/contacts',
        },
        {
          key: 'media-about-us',
          title: 'СМИ о нас',
          path: '/info/media',
        },
        {
          key: 'media-kit',
          title: 'Медиа-кит',
          path: '/info/media-kit',
        },
        {
          key: 'schema-osi',
          title: 'Модель OSI',
          path: '/info/schema-osi',
        },
      ],
    },
  ],
  en: [
    {
      key: 'our_services',
      title: 'Products & Solutions',
      sublinks: [
        {
          key: 'web',
          title: 'Website protection and optimization',
          path: '/store/web',
        },
        {
          key: 'hosting',
          title: 'Protected web hosting',
          path: '/store/hosting',
        },
        {
          key: 'vds',
          title: 'Protected VDS',
          path: '/store/vds',
        },
        {
          key: 'server',
          title: 'Protected dedicated servers',
          path: '/store/server',
        },
        {
          key: 'network_protection',
          title: 'Network protection',
          path: '/store/network-protection',
        },
        {
          key: 'bot-mitigation',
          title: 'Bot Mitigation',
          path: '/store/bot-mitigation',
        },
        { key: 'waf', title: 'WAF', path: '/store/waf' },
        // {
        //   key: 'ssl',
        //   title: 'SSL certificates',
        //   path: '/store/ssl',
        // },
      ],
    },
    {
      key: 'support',
      title: 'Support',
      sublinks: [
        { key: 'manual', title: 'Tutorials', path: '/manual' },
        ...documents.en,
        {
          key: 'terminology',
          title: 'Glossary',
          path: '/wiki/terminology',
        },
        {
          key: 'technologies',
          title: 'Technologies',
          path: '/wiki/technologies',
        },
      ],
    },
    {
      key: 'ddos-guard',
      title: 'DDoS-Guard',
      sublinks: [
        {
          key: 'about_us',
          title: 'About us',
          path: '/info/about-us',
        },
        {
          key: 'vacancy',
          title: 'Jobs',
          path: '/info/vacancy',
        },
        { key: 'blog', title: 'Blog', path: '/info/blog' },
        {
          key: 'contacts',
          title: 'Contacts',
          path: '/info/contacts',
        },
        {
          key: 'media-about-us',
          title: 'Media about us',
          path: '/info/media',
        },
        {
          key: 'media-kit',
          title: 'Media kit',
          path: '/info/media-kit',
        },
        {
          key: 'schema-osi',
          title: 'OSI model',
          path: '/info/schema-osi',
        },
      ],
    },
  ],
});

export { data, navigations };
