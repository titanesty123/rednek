import Base from '~/services/base';
import { data, navigations } from './const';

export default class FooterApiService extends Base {
  static getNavigations(locale) {
    return navigations[locale];
  }

  static getData(locale) {
    return data[locale];
  }
}
