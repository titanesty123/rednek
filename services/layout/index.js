import { HeaderApiService } from './header';
import { FooterApiService } from './footer';

export { HeaderApiService, FooterApiService };
