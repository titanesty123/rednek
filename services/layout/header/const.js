const navigations = Object.freeze({
  ru: [
    {
      key: 'our_services',
      title: 'Продукты и решения',
      groups: [
        {
          key: 'protection',
          title: 'Защитить',
          links: [
            {
              key: 'web',
              title: 'Сайт',
              description:
                'Оптимизируем работу вашего сайта и обеспечим его максимальную доступность',
              path: '/store/web',
            },
            {
              key: 'network_protection',
              title: 'Сеть',
              description:
                'Обеспечим защиту сетевой инфраструктуры и оптимальную маршрутизацию трафика',
              path: '/store/network-protection',
            },
          ],
        },
        {
          key: 'rent',
          title: 'Арендовать',
          links: [
            {
              key: 'hosting',
              title: 'Хостинг',
              description:
                'Разместите свой сайт на хостинге с защитой от DDoS-атак',
              path: '/store/hosting',
            },
            {
              key: 'vds',
              title: 'VDS',
              description:
                'Виртуальный сервер с защитой от всех известных DDoS-атак любой мощности',
              path: '/store/vds',
            },
            {
              key: 'server',
              title: 'Выделенные серверы',
              description:
                'Отдельный (физический) сервер с защитой от всех известных DDoS-атак любой мощности',
              path: '/store/server',
            },
          ],
        },
        {
          key: 'additional_services',
          title: 'Дополнительные услуги',
          links: [
            // {
            //   key: 'ssl',
            //   title: 'SSL-сертификаты',
            //   description: '',
            //   path: '/store/ssl',
            // },
            {
              key: 'bot-mitigation',
              title: 'Bot Mitigation',
              description:
                'Обезопасьте ваш ресурс от ботов и нежелательных посещений',
              path: '/store/bot-mitigation',
            },
            {
              key: 'waf',
              title: 'WAF',
              description:
                'Обезопасьте ваш бизнес от вредоносного вторжения и потери прибыли',
              path: '/store/waf',
            },
          ],
        },
      ],
    },
    {
      key: 'information',
      title: 'Информация',
      groups: [
        {
          key: null,
          title: null,
          links: [
            {
              key: 'about_us',
              title: 'О компании',
              description:
                'DDoS-Guard — оператор связи, занимающий лидирующие позиции на рынке услуг по обеспечению защиты от DDoS-атак и доставке контента с 2011 года',
              path: '/info/about-us',
            },
            {
              key: 'media-about-us',
              title: 'СМИ о нас',
              description:
                'Публикации о нашей компании, интервью и другие материалы',
              path: '/info/media',
            },
            {
              key: 'vacancy',
              title: 'Вакансии',
              description:
                'Присоединяйся к команде DDoS-Guard и получи безграничные возможности для карьерного роста и самореализации',
              path: '/info/vacancy',
            },
            {
              key: 'media-kit',
              title: 'Медиа-кит',
              description:
                'Материалы для наших клиентов или партнеров по бизнесу',
              path: '/info/media-kit',
            },
            {
              key: 'contacts',
              title: 'Контакты',
              description:
                'Номера телефонов горячей линии и реквизиты DDoS-Guard',
              path: '/info/contacts',
            },
            {
              key: 'documents',
              title: 'Документы',
              description: 'Документы',
              path: '/info/documents',
            },
          ],
        },
      ],
    },
    {
      key: 'knowledge_base',
      title: 'База знаний',
      groups: [
        {
          key: null,
          title: null,
          links: [
            {
              key: 'manual',
              title: 'Инструкции',
              description:
                'Подробная информация о подключении и настройке сервисов защиты DDoS-Guard',
              path: '/wiki/manual',
            },
            {
              key: 'terminology',
              title: 'Термины',
              description:
                'Значения используемых нами терминов, которые помогут лучше ориентироваться в области DDoS-защиты',
              path: '/wiki/terminology',
            },
            {
              key: 'technologies',
              title: 'Технологии',
              description:
                'Краткое описание технологий, на которых построены наши разработки',
              path: '/wiki/technologies',
            },
            {
              key: 'faq',
              title: 'FAQ',
              description:
                'Ответы на часто задаваемые вопросы, связанные с сервисами DDoS-Guard',
              path: '/wiki/faq',
            },
            {
              key: 'schema-osi',
              title: 'Модель OSI',
              description:
                'Сетевая модель стека сетевых протоколов OSI и визуализация воздействия вредоносного трафика',
              path: '/info/schema-osi',
            },
          ],
        },
      ],
    },
    {
      key: 'blog',
      title: 'Блог',
      path: '/info/blog',
      groups: [],
    },
  ],
  en: [
    {
      key: 'our_services',
      title: 'Products & Solutions',
      groups: [
        {
          key: 'protection',
          title: 'Protect',
          links: [
            {
              key: 'web',
              title: 'Website',
              description:
                'We will improve the performance of your website and make it fully accessible',
              path: '/store/web',
            },
            {
              key: 'network_protection',
              title: 'Network',
              description:
                'DDoS-Guard ensures protection of network infrastructure and effective traffic routing',
              path: '/store/network-protection',
            },
          ],
        },
        {
          key: 'rent',
          title: 'Rent',
          links: [
            {
              key: 'hosting',
              title: 'Web Hosting',
              description:
                'Use a web hosting service with DDoS protection. Let us ensure the reliability and quick loading of your project',
              path: '/store/hosting',
            },
            {
              key: 'vds',
              title: 'VDS',
              description:
                'Virtual server with comprehensive protection against all known DDoS attack types of any power',
              path: '/store/vds',
            },
            {
              key: 'server',
              title: 'Dedicated Servers',
              description:
                'Dedicated (physical) server with comprehensive protection against all known DDoS attack types of any power',
              path: '/store/server',
            },
          ],
        },
        {
          key: 'additional_services',
          title: 'Additional Services',
          links: [
            // {
            //   key: 'ssl',
            //   title: 'SSL certificates',
            //   description: '',
            //   path: '/store/ssl',
            // },
            {
              key: 'bot-mitigation',
              title: 'Bot Mitigation',
              description:
                'Prevent your resource from being accessed by bots and unwanted visitors',
              path: '/store/bot-mitigation',
            },
            {
              key: 'waf',
              title: 'WAF',
              description:
                'Prevent your business from malicious cyber intrusions and loss of profit',
              path: '/store/waf',
            },
          ],
        },
      ],
    },
    {
      key: 'information',
      title: 'Information',
      groups: [
        {
          key: null,
          title: null,
          links: [
            {
              key: 'about_us',
              title: 'About Us',
              description:
                'DDoS-Guard is one of the leading Internet Service Providers specialized in DDoS protection and global content delivery since 2011',
              path: '/info/about-us',
            },
            {
              key: 'media-about-us',
              title: 'Media About Us',
              description:
                'Articles featuring our company, interviews, and related materials',
              path: '/info/media',
            },
            {
              key: 'vacancy',
              title: 'Career',
              description:
                'Join DDoS-Guard team and get infinite opportunities for career growth',
              path: '/info/vacancy',
            },
            {
              key: 'media-kit',
              title: 'Media Kit',
              description: 'Materials for our customers and business partners',
              path: '/info/media-kit',
            },
            {
              key: 'contacts',
              title: 'Contacts',
              description:
                "Hotline phone numbers and DDoS-Guard's legal details",
              path: '/info/contacts',
            },
            {
              key: 'documents',
              title: 'Documents',
              description: 'Documents',
              path: '/info/documents',
            },
          ],
        },
      ],
    },
    {
      key: 'knowledge_base',
      title: 'Knowledge Base',
      groups: [
        {
          key: null,
          title: null,
          links: [
            {
              key: 'manual',
              title: 'Tutorials',
              description:
                'Detailed information on how to activate and configure our services',
              path: '/wiki/manual',
            },
            {
              key: 'terminology',
              title: 'Glossary',
              description:
                'Terminology we use to describe how our services work',
              path: '/wiki/terminology',
            },
            {
              key: 'technologies',
              title: 'Technologies',
              description:
                'An overview of the technologies we use in our products',
              path: '/wiki/technologies',
            },
            {
              key: 'faq',
              title: 'FAQ',
              description:
                'A list of commonly asked questions and answers related to our services',
              path: '/wiki/faq',
            },
            {
              key: 'schema-osi',
              title: 'OSI Model',
              description:
                'The Open Systems Interconnection model and the visualization of malicious traffic impact',
              path: '/info/schema-osi',
            },
          ],
        },
      ],
    },
    {
      key: 'blog',
      title: 'Blog',
      path: '/info/blog',
      groups: [],
    },
  ],
});

export { navigations };
