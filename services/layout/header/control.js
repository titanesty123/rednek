import Base from '~/services/base';
import { navigations } from './const';

export default class HeaderApiService extends Base {
  static getNavigations(locale) {
    return navigations[locale];
  }
}
