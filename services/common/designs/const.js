const filters = [
    {
        name: 'Все проекты',
        code: 'all',
        elements: [],
    },
    {
        name: 'Жилые',
        code: 'private',
        elements: [
            {
                name: 'Все жилые',
                code: 'all'
            },
            {
                name: 'Дома',
                code: 'house'
            },
            {
                name: 'Квартиры',
                code: 'flat'
            },
            {
                name: 'Дачи',
                code: 'dacha'
            }
        ]
    },
    {
        name: 'Коммерческие',
        code: 'commercial',
        elements: [
            {
                name: 'Все коммерческие',
                code: 'all'
            },
            {
                name: 'Кафе',
                code: 'caffe'
            },
            {
                name: 'Оффисы',
                code: 'office'
            },
        ]
    }
]

const data = [
    {
        title: 'Дом в Москве в стиле борокко 1',
        desc: 'Banjo tote bag bicycle rights, High Life sartorial cray craft beer whatever street art fap. Hashtag typewriter banh mi, squid keffiyeh High Life Brooklyn twee craft beer tousled chillwave. PBR&B selfies chillwave, bespoke tote bag blog post-ironic. Single-origin coffee put a bird on it craft beer YOLO, Portland hella deep v Schlitz. Tumblr Bushwick post-ironic Thundercats. Vinyl 90\'s keytar, literally cardigan Williamsburg YOLO squid pickled Etsy salvia lo-fi locavore. Meh leggings retro narwhal Neutra.',
        area: '100',
        src: 'portfolio_01.png',
        year: '2021',
        address: 'посёлок "HONKA" Медное озеро',
        id: 1,
        code: 'house',
        category: 'private',
    },
    {
        title: 'Квартира на Пушкинской 42 2',
        desc: 'Banjo tote bag bicycle rights, High Life sartorial cray craft beer whatever street art fap. Hashtag typewriter banh mi, squid keffiyeh High Life Brooklyn twee craft beer tousled chillwave. PBR&B selfies chillwave, bespoke tote bag blog post-ironic. Single-origin coffee put a bird on it craft beer YOLO, Portland hella deep v Schlitz. Tumblr Bushwick post-ironic Thundercats. Vinyl 90\'s keytar, literally cardigan Williamsburg YOLO squid pickled Etsy salvia lo-fi locavore. Meh leggings retro narwhal Neutra.',
        area: '58',
        code: 'flat',
        category: 'private',
        src: 'portfolio_02.png',
        id: 2,
        year: '2023',
        address: 'посёлок "HONKA" Медное озеро'
    },
    {
        title: 'Оффис в Санкт-Петербурге в современном стиле 3',
        desc: 'Banjo tote bag bicycle rights, High Life sartorial cray craft beer whatever street art fap. Hashtag typewriter banh mi, squid keffiyeh High Life Brooklyn twee craft beer tousled chillwave. PBR&B selfies chillwave, bespoke tote bag blog post-ironic. Single-origin coffee put a bird on it craft beer YOLO, Portland hella deep v Schlitz. Tumblr Bushwick post-ironic Thundercats. Vinyl 90\'s keytar, literally cardigan Williamsburg YOLO squid pickled Etsy salvia lo-fi locavore. Meh leggings retro narwhal Neutra.',
        area: '200',
        code: 'office',
        category: 'commercial',
        src: 'portfolio_03.png',
        id: 3,
        year: '2025',
        address: 'посёлок "HONKA" Медное озеро'
    },
    {
        title: 'Дом в Москве в стиле неокласика 4',
        desc: 'Banjo tote bag bicycle rights, High Life sartorial cray craft beer whatever street art fap. Hashtag typewriter banh mi, squid keffiyeh High Life Brooklyn twee craft beer tousled chillwave. PBR&B selfies chillwave, bespoke tote bag blog post-ironic. Single-origin coffee put a bird on it craft beer YOLO, Portland hella deep v Schlitz. Tumblr Bushwick post-ironic Thundercats. Vinyl 90\'s keytar, literally cardigan Williamsburg YOLO squid pickled Etsy salvia lo-fi locavore. Meh leggings retro narwhal Neutra.',
        area: '103',
        code: 'house',
        category: 'private',
        src: 'portfolio_04.png',
        id: 4,
        year: '2021',
        address: 'посёлок "HONKA" Медное озеро'
    },
    {
        title: 'Дача в Москве в стиле неокласика 5',
        desc: 'Banjo tote bag bicycle rights, High Life sartorial cray craft beer whatever street art fap. Hashtag typewriter banh mi, squid keffiyeh High Life Brooklyn twee craft beer tousled chillwave. PBR&B selfies chillwave, bespoke tote bag blog post-ironic. Single-origin coffee put a bird on it craft beer YOLO, Portland hella deep v Schlitz. Tumblr Bushwick post-ironic Thundercats. Vinyl 90\'s keytar, literally cardigan Williamsburg YOLO squid pickled Etsy salvia lo-fi locavore. Meh leggings retro narwhal Neutra.',
        area: '333',
        code: 'dacha',
        category: 'private',
        src: 'portfolio_01.png',
        id: 5,
        year: '2021',
        address: 'посёлок "HONKA" Медное озеро'
    },
    {
        title: 'Кафе в Санкт-Петербурге в современном стиле 3',
        desc: 'Banjo tote bag bicycle rights, High Life sartorial cray craft beer whatever street art fap. Hashtag typewriter banh mi, squid keffiyeh High Life Brooklyn twee craft beer tousled chillwave. PBR&B selfies chillwave, bespoke tote bag blog post-ironic. Single-origin coffee put a bird on it craft beer YOLO, Portland hella deep v Schlitz. Tumblr Bushwick post-ironic Thundercats. Vinyl 90\'s keytar, literally cardigan Williamsburg YOLO squid pickled Etsy salvia lo-fi locavore. Meh leggings retro narwhal Neutra.',
        area: '200',
        code: 'caffe',
        category: 'commercial',
        src: 'portfolio_03.png',
        id: 6,
        year: '2025',
        address: 'посёлок "HONKA" Медное озеро'
    },
]

export {data, filters};
